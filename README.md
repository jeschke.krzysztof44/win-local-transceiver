# WinLocalTransceiver

React Native and Python apps which allow user to transfer files between Windows and smartphone while being connected to the same local network.

## Python server
FTP server created using Python. It automatically enables FTP protocol in Windows Defender. User can choose destination folder path which will be saved for future uploadings.

![image.png](./image.png)

## React Native app
FTP client app. User can choose multiple files to send to selected folder in Python server.
