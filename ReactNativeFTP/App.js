import React, {Component} from 'react';
import {StyleSheet, View, Button} from 'react-native';
import FTP from 'react-native-ftp';

import {SafeAreaProvider} from 'react-native-safe-area-context';

import MainScreen from './src/screens/MainScreen';

export default class App extends Component {
  render() {
    return (
      <SafeAreaProvider>
        <MainScreen />
        {/* <StatusBar /> */}
      </SafeAreaProvider>
    );
  }
}
