import FTP from 'react-native-ftp';

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export class UploadFTP {
  constructor(IP, port, username, password) {
    this._IP = IP;
    this._port = port;
    this._username = username;
    this._password = password;
    this._transferCompleted = false;
  }
  async connectAndUploadMultiple(filesPath) {
    try {
      FTP.setup(this._IP, this._port); //Setup host
      FTP.login(this._username, this._password).then(
        result => {
          FTP.uploadMultipleFiles(filesPath, './').then(
            result => {
              this._transferCompleted = true;
              //alert('File transfer completed.');
            },
            error => {
              alert(error);
            },
          );
        },
        error => {
          alert(error);
        },
      );
    } catch (err) {
      alert('Error while sending file.');
    }
  }
  getTransferCompleted() {
    return this._transferCompleted;
  }
  resetTransferCompleted() {
    this._transferCompleted = false;
  }
}
