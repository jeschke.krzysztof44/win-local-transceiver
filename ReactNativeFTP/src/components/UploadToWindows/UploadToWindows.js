import React, {useState} from 'react';
import {StyleSheet, View, Dimensions, Modal, Text} from 'react-native';

import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import RNFetchBlob from 'react-native-fetch-blob';
import AnimatedLoader from 'react-native-animated-loader';
import BackgroundService from 'react-native-background-actions';

import {UploadFTP} from '../../class/ConnectFTP';
import DocumentPicker from 'react-native-document-picker';

var screenWidth = Dimensions.get('screen').width;
var screenHeight = Dimensions.get('screen').height;

export default function UploadToWindows(props) {
  var {userName, password, ip, port} = props;

  const [loadingVisible, setLoadingVisible] = useState(false);
  const [completedVisible, setcompletedVisible] = useState(false);

  var filesUri = [];
  var filesPath = [];

  const sleep = time =>
    new Promise(resolve => setTimeout(() => resolve(), time));

  const options = {
    taskName: 'LoadCheck',
    taskTitle: 'LoadCheck',
    taskDesc: 'LoadCheck',
    taskIcon: {
      name: 'ic_launcher',
      type: 'mipmap',
    },
  };

  const selectAndUpload = async () => {
    await selectFile();

    if (filesUri.length > 0) {
      try {
        for (const uri of filesUri) {
          await RNFetchBlob.fs
            .stat(uri)
            .then(stats => {
              filesPath.push(stats.path);
            })
            .catch(err => {
              console.log(err);
            });
        }
        if (filesPath.length > 0) {
          ftp = new UploadFTP(ip, parseInt(port), userName, password);
          ftp.connectAndUploadMultiple(filesPath);
          setLoadingVisible(true);
          const checkLoad = async taskDataArguments => {
            await new Promise(async resolve => {
              while (BackgroundService.isRunning()) {
                if (ftp.getTransferCompleted()) {
                  setcompletedVisible(true);
                  setLoadingVisible(false);
                  await sleep(3000);
                  setcompletedVisible(false);
                  ftp.resetTransferCompleted();
                  BackgroundService.stop();
                }
                await sleep(100);
              }
            });
          };
          BackgroundService.start(checkLoad, options);
        } else {
          console.log('Path is null');
        }
      } catch (err) {
        console.log(err);
        alert('File transfer failed.');
      }
      filesUri = [];
      filesPath = [];
    }
  };

  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      for (const file of res) {
        filesUri.push(file.uri);
      }
      //filesUri = res.uri;
    } catch (err) {
      filesUri = [];
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };
  return (
    <View style={styles.container}>
      {/* <Modal transparent={true} visible={true}>
        <View style={styles.popUpContainer}>
          <View style={styles.popUpInside}> */}
      <AnimatedLoader
        visible={loadingVisible}
        overlayColor="rgba(255,255,255,0.8)"
        source={require('./loader.json')}
        animationStyle={styles.loaderStyle}
        speed={1}>
        <Text style={styles.popUpText}>Uploading files...</Text>
      </AnimatedLoader>
      <AnimatedLoader
        visible={completedVisible}
        overlayColor="rgba(255,255,255,0.8)"
        source={require('./tick.json')}
        animationStyle={styles.completedStyle}
        speed={1}>
        <Text style={styles.popUpText}>File transfer completed!</Text>
      </AnimatedLoader>
      {/* </View>
        </View>
      </Modal> */}
      <Button
        buttonStyle={styles.buttonStyle}
        title="Upload files to windows"
        raised={true}
        onPress={selectAndUpload}
        icon={
          <Icon
            name="arrow-circle-o-up"
            size={Math.round(screenWidth * 0.19)}
            color="white"
            style={styles.iconStyle}
          />
        }></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    //elevation: 5,
    //padding: 10,
    margin: 5,

    //borderWidth: 1,
    //borderRadius: 15,

    // maxHeight: "50%",
    // minWidth: "50%",
    // maxWidth: "50%",

    justifyContent: 'center',
  },
  popUpContainer: {
    //flex: 1,
  },
  popUpInside: {
    //paddingTop: '40%',
    marginTop: '70%',
    marginLeft: '5%',
    marginRight: '5%',
    marginBottom: '70%',

    borderRadius: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: 'white',
    borderBottomWidth: 10,
  },
  loaderStyle: {
    width: 100,
    height: 100,
  },
  completedStyle: {
    width: 100,
    height: 100,
  },
  popUpText: {
    fontSize: 30,
  },
  buttonStyle: {
    width: Math.round(screenWidth * 0.55),
    height: Math.round(screenHeight * 0.35),
    flexDirection: 'column',
  },
  iconStyle: {
    //width: Math.round(screenWidth * 0.4),
    //height: Math.round(screenHeight * 0.25),
    //position: "absolute",
    //top: 100,
  },
});
