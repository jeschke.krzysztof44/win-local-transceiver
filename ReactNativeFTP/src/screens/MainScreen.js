import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Modal,
  Dimensions,
} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';

import UploadToWindows from '../components/UploadToWindows/UploadToWindows';

var screenWidth = Dimensions.get('screen').width;

export default class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: 'krzysztof',
      password: '1234',
      ip: '192.168.0.11',
      port: '21',
      qrScan: false,
    };
  }
  onSuccess = e => {
    console.log(e.data);
    json_data = JSON.parse(e.data);
    this.setState({
      userName: json_data['user'],
      password: json_data['password'],
      ip: json_data['ip_address'],
      port: json_data['port'],
      qrScan: false,
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <Button
          buttonStyle={styles.buttonStyle}
          title="Scan QR code"
          raised={true}
          onPress={() => {
            this.setState({qrScan: true});
          }}
          icon={
            <Icon
              name="camera"
              size={Math.round(screenWidth * 0.1)}
              color="white"
              //style={styles.iconStyle}
            />
          }></Button>
        <Modal transparent={true} visible={this.state.qrScan}>
          <View style={styles.qrStyle}>
            <QRCodeScanner
              onRead={this.onSuccess}
              //flashMode={RNCamera.Constants.FlashMode.torch}
              //topContent={}
              bottomContent={
                <Button
                  buttonStyle={styles.qrBottomButton}
                  title="Go back"
                  raised={true}
                  onPress={() => {
                    this.setState({qrScan: false});
                  }}></Button>
              }
            />
          </View>
        </Modal>
        <View style={styles.boxesStyle}>
          <UploadToWindows
            userName={this.state.userName}
            password={this.state.password}
            ip={this.state.ip}
            port={this.state.port}></UploadToWindows>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column-reverse',
  },
  boxesStyle: {
    paddingTop: Math.round(screenWidth / 2),
    //flex: 1,
    flexWrap: 'wrap',
    //backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonStyle: {
    flexDirection: 'column',
    backgroundColor: 'darkgreen',
  },
  qrStyle: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    //opacity: 0.5,
  },
  qrBottomButton: {
    padding: 7,
    paddingRight: 30,
    paddingLeft: 30,
    backgroundColor: 'green',
    //opacity: 0.5,
  },
});
