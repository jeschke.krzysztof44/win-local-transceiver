from tkinter import *
from tkinter.filedialog import askdirectory

import tkinter as tk
import os, sys
import pyqrcode
import json
import threading

import time

from winTransceiverFTP import WinTransceiverFTP

JSON_PATH_NAME = "path.json"


def generate():
    global qr, photo
    qr = pyqrcode.create(json.dumps(SERVER_DATA))
    photo = BitmapImage(data=qr.xbm(scale=8))

    showcode()


def showcode():
    imageLabel.config(image=photo)
    subLabel.config(text="Scan QR code to get server data")


def choosePath():
    dir = askdirectory()
    if not dir:
        dir = r"./"
    saveJSONPath(dir)
    ftp.setFolderPath(dir)
    pathLabel["text"] = ftp._foler_path
    ftp.changeUser()


def saveJSONPath(path):
    data = {"path": path}
    with open(JSON_PATH_NAME, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def openLastPath():
    if os.path.isfile(JSON_PATH_NAME):
        with open(JSON_PATH_NAME, "r", encoding="utf-8") as f:
            data = json.load(f)
            ftp.setFolderPath(data["path"])
    else:
        path = r"./"
        saveJSONPath(path)
        ftp.setFolderPath(path)


def on_closing():
    try:
        ftp._server.close_all()
    except:
        pass
    window.destroy()


window = Tk()
window.title("Windows FTP Server")

imageLabel = Label(window)
imageLabel.grid(row=0, column=1, sticky=N + S + W + E)

subLabel = Label(window, text="")
subLabel.grid(row=1, column=1, sticky=N + S + W + E)

pathLabel = Label(window, text="")
pathLabel.grid(row=2, column=1, sticky=N + S + W + E)


button = Button(
    window,
    text="Select FTP folder path",
    width=15,
    height=2,
    command=choosePath,
    bg="#2ab023",
)
button.grid(row=3, column=1, sticky=N + S + W + E)

# SubEntry = Text(window)
# SubEntry.configure(state="normal")
# SubEntry.insert(tk.END, "testtt")
# SubEntry.configure(state="disabled")
# SubEntry.grid(row=2, column=1, sticky=N + S + W + E)

# making this resposnsive
Rows = 3
Columns = 3

for row in range(Rows + 1):
    window.grid_rowconfigure(row, weight=1)

for col in range(Columns + 1):
    window.grid_columnconfigure(col, weight=1)

ftp = WinTransceiverFTP()

SERVER_DATA = ftp.getServerData()
openLastPath()
pathLabel["text"] = ftp._foler_path

ftpThread = threading.Thread(target=ftp.runServer)

ftpThread.start()

generate()
window.protocol("WM_DELETE_WINDOW", on_closing)
window.mainloop()