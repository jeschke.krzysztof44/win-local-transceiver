from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

import subprocess, ctypes, os, sys
from subprocess import Popen, DEVNULL, check_output

import socket

RULE_NAME = "WinTransceiverFTP"  # Has to be same as in defender.py (subprocess didn't work with params -.-)
EXE_NAME = "gui.exe"
# FOLDER_PATH = r"C://Users//jesch//Desktop//FTP_XIAOMI"

# For testing
# def getExePath():
#     """ Get .exe file path"""
#     exe_subpath = os.path.join(r"dist\main", EXE_NAME)
#     dir_path = os.path.dirname(os.path.realpath(__file__))
#     return os.path.join(dir_path, exe_subpath)


class WinTransceiverFTP:
    def __init__(self):
        self._ip_address = self.getIPAddress()
        self._port = "21"
        self._user = "user"
        self._password = "1234"
        self._foler_path = r"./"

        self._server_data = {
            "ip_address": self._ip_address,
            "port": self._port,
            "user": self._user,
            "password": self._password,
        }

        self.checkDefender()

        self._authorizer = DummyAuthorizer()
        self._handler = FTPHandler
        self._server = None

    def getServerData(self):
        return self._server_data

    def setFolderPath(self, foler_path):
        self._foler_path = foler_path

    def changeUser(self):
        self._authorizer.remove_user(self._user)
        self._authorizer.add_user(
            self._user, self._password, self._foler_path, perm="elradfmwMT"
        )

    def runServer(self):
        self._authorizer.add_user(
            self._user, self._password, self._foler_path, perm="elradfmwMT"
        )
        # self._authorizer.add_anonymous(self._foler_path)

        self._handler.authorizer = self._authorizer

        self._server = FTPServer((self._ip_address, self._port), self._handler)
        self._server.serve_forever()

    # # def checkRule(self):
    # #     """ Check if Windows Firewall rule exists"""
    # #     return subprocess.call(
    # #         f".\checkRule.ps1",
    # #         shell=True,
    # #         # stdout=DEVNULL,
    # #         # stderr=DEVNULL,
    # #     )

    def getExePath(self):
        """ Get .exe file path"""
        dir_path = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(dir_path, EXE_NAME)

    def checkRule(self):
        """ Check if Windows Firewall rule exists"""
        return subprocess.check_output(
            f"..\..\checkRule.ps1",
            shell=True,
            stderr=DEVNULL,
        )

    def checkDefender(self):
        """ Check if Windows Firewall rule exists. Add new if not found rule name """
        rule = self.checkRule()
        if not int(rule):
            cmd_command = f"Start-Process ..\..\defender\dist\defender\defender.exe -verb runas -ArgumentList ('{self.getExePath()} {RULE_NAME}')"
            subprocess.call(
                # f"..\..\defender\dist\defender\defender.exe {self.getExePath()} {RULE_NAME}"
                f'powershell -Command "{cmd_command}"',
                shell=True,
            )

    def getIPAddress(self):
        """" Get Host IP address"""
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip_addr = s.getsockname()[0]
        s.close()
        return ip_addr


# ftp = WinTransceiverFTP()
# ftp.runServer()
# checkDefender()

# IP_ADDRESS = getIPAddress()
# PORT = 21


# print(IP_ADDRESS)

# FOLDER_PATH = r"C://Users//jesch//Desktop//FTP_XIAOMI"

# authorizer = DummyAuthorizer()
# authorizer.add_user("user", "1234", FOLDER_PATH, perm="elradfmwMT")
# authorizer.add_anonymous(FOLDER_PATH)

# handler = FTPHandler
# handler.authorizer = authorizer

# server = FTPServer((IP_ADDRESS, PORT), handler)
# server.serve_forever()