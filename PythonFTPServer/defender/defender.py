import subprocess, ctypes, os, sys
from subprocess import Popen, DEVNULL, check_output


def chkAdmin():
    """ Force to start application with admin rights """
    try:
        isAdmin = ctypes.windll.shell32.IsUserAnAdmin()
    except AttributeError:
        isAdmin = False
    if not isAdmin:
        ctypes.windll.shell32.ShellExecuteW(
            None, "runas", sys.executable, __file__, None, 1
        )


def addRule(exe_path, rule_name):
    """ Add rule to Windows Firewall """
    subprocess.call(
        f"netsh advfirewall firewall add rule name={'WinTransceiverFTP'} dir=in action=allow enable=yes program={exe_path}",
        shell=True,
        # stdout=DEVNULL,
        # stderr=DEVNULL,
    )


# def checkRule():
#     """ Check if Windows Firewall rule exists"""
#     return subprocess.check_output(
#         f"..\..\checkRule.ps1",
#         shell=True,
#         # stdout=DEVNULL,
#         stderr=DEVNULL,
#     )


# def checkDefender(exe_path):
#     """ Check if Windows Firewall rule exists. Add new if not found rule name """
#     rule = checkRule()
#     # print(int(rule))
#     if not int(rule):
#         chkAdmin()
#         addRule(exe_path)


if __name__ == "__main__":
    exe_path = sys.argv[1]
    rule_name = sys.argv[2]

    addRule(exe_path, rule_name)
