$ruleName = "WinTransceiverFTP" # Has to be same as in defender.py (subprocess didn't work with params -.-)

if ($(Get-NetFirewallRule -DisplayName $ruleName | Get-NetFirewallPortFilter)) {
    write-host "1"
}
else {
    Write-Host "0"
}